# this script yields for each high-level tag of the tagged_subpages_1.csv dataset,
# the weights yielded by the w2v original model (weights for curlie classes).

from typing import Dict, List
from original_w2v.model import WebsiteClassifier
import pandas as pd


class WeightedWebpage:
    def __init__(self, url, our_tag=None, curlie_tags={}):
        self.url = url
        self.our_tag = our_tag
        self.curlie_tags = curlie_tags


def add_combination(our_tag, tag_pages_weights: List[Dict[str,float]], all_combinations: Dict[str,Dict[str,float]]):
    avg_tag_weights = tag_pages_weights[0]
    for _, weights in enumerate(tag_pages_weights, 1):
        for curlie_tag, weight in weights.items():
            avg_tag_weights[curlie_tag] += weight
    all_combinations[our_tag] = {curlie_tag: weight/len(tag_pages_weights) for curlie_tag, weight in avg_tag_weights.items()}
    print('\nCOMBINATIONS SO FAR:')
    print(all_combinations)


if __name__ == '__main__':
    file_reader = pd.read_csv('../data/tagged_subpages_1.csv')
    urls = list(file_reader.iloc[1:, 0].values)
    tags = list(file_reader.iloc[1:, 1].values)
    our_tags = list(set(tags))
    webpages = []
    for i, url in enumerate(urls):
        w = WeightedWebpage(url, tags[i])
        webpages.append(w)
    webpages.sort(key=lambda w: w.our_tag)
    w2v_curlie_model = WebsiteClassifier(device='cpu')
    curr_tag = webpages[0].our_tag
    print('Current Tag: ' + curr_tag + '\n')
    combinations = {tag: {} for tag in our_tags}
    curr_tag_weights = []
    for i, w in enumerate(webpages):
        if w.our_tag != curr_tag:
            add_combination(curr_tag, curr_tag_weights, combinations)
            curr_tag = w.our_tag
            curr_tag_weights = []
            print('\nCurrent Tag: ' + curr_tag)
        print('(' + str(i+1) + '/' + str(len(webpages)) + ') - ' + w.url)
        w2v_w = w2v_curlie_model.fetch_website(w.url)
        if (w2v_w is None or not w2v_w.is_valid):
            continue
        scores, embeddings = w2v_curlie_model.predict(w2v_w)
        w.curlie_tags = scores
        curr_tag_weights.append(w.curlie_tags)
    add_combination(curr_tag, curr_tag_weights, combinations)