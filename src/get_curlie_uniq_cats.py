import pandas as pd


if __name__ == '__main__':
    file_reader = pd.read_csv('../data/curlie_filtered.csv')
    uniq_cats = list(set(file_reader.iloc[1:, 3].values))
    en_uniq_cats = []
    for cat in uniq_cats:
        if '/en/' in cat:
            en_uniq_cats.append(cat)
    en_uniq_cats.sort()
    print('uniq categories: ')
    for i, cat in enumerate(en_uniq_cats):
        print(str(i) + ': ', cat)
    print('tamany: ', str(len(en_uniq_cats)))
    print('type: ', type(en_uniq_cats[0]))
    