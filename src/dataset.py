# following this tutorial to load my dataset:
# https://shashikachamod4u.medium.com/excel-csv-to-pytorch-dataset-def496b6bcc1

# CUSTOM MADE FOR:
# __init__('../datasets/URL-categorization-DFE.csv')

from itertools import dropwhile, takewhile
import json
import math
import os
from typing import Dict, List, Tuple
import pandas as pd
import torch
from torch.utils.data import Dataset
from model import Webpage, WebsiteClassifier


class CSVDataset(Dataset):
    # ids: List[int] = []
    data: List[Tuple[str, str]] = []
    target_classes: List[str] = []
    class_to_number = {}
    num_samples = 0

    def __init__(self, file_path, model: WebsiteClassifier, classes, fetch_unfetched_urls=True, max_rows=None):
        self.model = model
        self.classes = classes
        self.max_rows = max_rows
        self._read_csv(file_path, fetch_unfetched_urls)


    def __len__(self):
        return self.num_samples


    def __getitem__(self, index) -> Tuple[torch.Tensor, torch.Tensor]:
        return self.data[index]


    def __get_page_prepared_for_training(self, webpage: Webpage):
        webpage.features = self.model.get_features(
                webpage.url, webpage.html, webpage.screenshot_path)
        input_features = self.model.concatenate_features(webpage)
        class_id = self.class_to_number[webpage.label]
        return (
            torch.FloatTensor(input_features),
            torch.tensor([class_id])
        )


    def _fetch_urls(self, samples, cached_pages_file_path, webpages_data=[]):
        print('fetching urls...')

        for row in samples:
            page_data = self.model.fetch_website(row.url, row._1, row.main_category)
            print('Dataset row: ' + str(row.Index) + ', uid: ' + str(row._1) + ' - ', end='')
            if not page_data.is_valid:
                print('Skipping ', page_data.url)
                continue
            print('Fetching ', page_data.url)
            webpages_data.append(page_data.__dict__)
            prepared_page = self.__get_page_prepared_for_training(page_data)
            self.data.append(prepared_page)
            # self.ids.append(row._1)
        
        with open(cached_pages_file_path, 'w') as f:
            json.dump(webpages_data, f)
    

    def _load_cached_pages(self, cached_pages_file_path, samples, skip_unfetched) -> List[Tuple[str, str]]:
        with open(cached_pages_file_path, 'r') as f:
            webpages_data = list(json.load(f))
        print('\nloading ' + str(min(len(webpages_data), len(samples))) + ' cached pages...\n')
        max_uid_cache = int(webpages_data[len(webpages_data) - 1]['uid'])

        max_uid_all = int(samples[len(samples) - 1]._1)

        unfetched_samples = list(dropwhile(lambda row: int(row._1) <= max_uid_cache, samples))
        
        for page_data in webpages_data:
            if page_data['uid'] > max_uid_all and max_uid_all < page_data['uid']:
                break                
            page = Webpage(
                page_data['url'],
                page_data['uid'],
                page_data['label'],
                page_data['is_valid'],
                page_data['http_code'],
                page_data['html'],
                page_data['screenshot_path'],
                page_data['features'],
                page_data['embedding'],
                page_data['scores']
            )
            # self.ids.append(page.uid)
            self.data.append(self.__get_page_prepared_for_training(page))

        if len(unfetched_samples) > 0 and not skip_unfetched:
            print(str(len(unfetched_samples)) + ' unfetched samples')
            self._fetch_urls(unfetched_samples, cached_pages_file_path, webpages_data)
    

    def _read_csv(self, file_path, fetch_unfetched_urls):
        print('\nreading dataset csv...\n')
        file_reader = pd.read_csv(file_path)

        print('indexing target classes...\n')
        self.target_classes = filter(
            lambda tag: tag in self.classes, list(set(file_reader.iloc[1:, 5].values)))
        for i, label in enumerate(self.target_classes):
            print(str(i) + ': ', label)
            self.class_to_number[label] = i
        
        samples = list(filter(
            lambda x: x.main_category in self.classes, file_reader.itertuples()))
        if self.max_rows is not None:
            samples = samples[:self.max_rows]
        print('original samples length: ', len(samples))
        # print('samples:')
        # print(list(map(lambda s: (s.Index, s._1), samples)))
        cached_pages_file_path = './__pages_cache' + '.json'
        os.makedirs(os.path.dirname(cached_pages_file_path), exist_ok=True)
        

        if (not os.path.isfile(cached_pages_file_path)):
            self._fetch_urls(samples, cached_pages_file_path)
        else:
            self._load_cached_pages(cached_pages_file_path, samples, not fetch_unfetched_urls)
        self.num_samples = len(self.data)
