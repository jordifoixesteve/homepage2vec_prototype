from datetime import datetime
import json
import math
import os

from sklearn.utils import shuffle
import torch
from model import WebsiteClassifier
from dataset import CSVDataset
from torch.utils.data import DataLoader, random_split
from torch.utils.tensorboard import SummaryWriter


# Version
# 1. Load pretrained model and train all layers
# 2. Load pretrained model and train only the new last layer
# 3. Train all layers from scratch
VERSION = 1

# Number of dataset rows to use, or None to use all
MAX_ROWS = 20000 #10000 #None
# Set to false to prevent from fetching urls. Will use only the cached pages data
FETCH_UNFETCHED = True
FILTERED_CLASSES = ['Autos_and_Vehicles', 'Recreation_and_Hobbies', 'Beauty_and_Fitness',
                    'Reference', 'Food_and_Drink', 'Career_and_Education', 'Business_and_Industry',
                    'Law_and_Government', 'Shopping', 'Games', 'News_and_Media', 'Finance',
                    'Travel', 'Home_and_Garden', 'Books_and_Literature', 'Pets_and_Animals',
                    'Internet_and_Telecom', 'Computer_and_Electronics', 'Gambling', 'Sports', 'Health',
                    'Science', 'Arts_and_Entertainment', 'People_and_Society']#, 'Not_working', 'Adult']


# Training Hyperparameters
NUM_EPOCHS = 10000
BATCH_SIZE = 32
LEARNING_RATE = 0.001
SAVE_EVERY_N_EPOCHS = 10
writer = SummaryWriter()


def train(model, train_loader, val_loader, loss_fn, optimizer_type, epochs=NUM_EPOCHS, shuffle=True, version=1):
    print('Training...')

    if version == 1 or version == 3:
        # all layers will be trained
        optimizer = optimizer_type(model.parameters(), lr=LEARNING_RATE)

    if version == 2:
        # freeze all layers but the last one
        model.layer1.weight.requires_grad = False
        model.layer2.weight.requires_grad = False
        # pass only those parameters that explicitly require gradients
        optimizer = optimizer_type(filter(lambda p: p.requires_grad, model.parameters()), lr=LEARNING_RATE)

    curr_exec_dir_path = '../version_' + str(version) + '/' + str(datetime.now().strftime("%Y%m%dT%H%M%S") + '/')
    os.makedirs(os.path.dirname(curr_exec_dir_path), exist_ok=True)
    
    for epoch in range(epochs):
        running_loss = 0.0
        curr_epoch_dir_path = curr_exec_dir_path + 'epoch_' + str(epoch) + '/'
        curr_epoch_models_dir_path = curr_epoch_dir_path + 'trained_models/'
        curr_epoch_stats_file_path = curr_epoch_dir_path + 'stats.json'
        curr_epoch_stats_file_path_eval = curr_epoch_dir_path + 'eval_stats.json'
        if epoch % SAVE_EVERY_N_EPOCHS == 0:
            os.makedirs(os.path.dirname(curr_epoch_models_dir_path), exist_ok=True)
            os.makedirs(os.path.dirname(curr_epoch_stats_file_path), exist_ok=True)
        model.train()
        for i, data_batch in enumerate(train_loader, 0):
            # data is a list of [inputs, labels]
            inputs, labels = data_batch
            
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs, grad_fn = model(inputs)
            loss = loss_fn(outputs, labels.squeeze(1))
            loss.backward()
            optimizer.step()

            # save trained model every 100 mini-batches
            running_loss += loss.item()
            if epoch % SAVE_EVERY_N_EPOCHS == 0 and i % 100 == 99:
                # save trained model
                curr_model_file_path = curr_epoch_models_dir_path + 'it_' + str(i) + '_model.pt'
                os.makedirs(os.path.dirname(curr_model_file_path), exist_ok=True)
                torch.save(model.state_dict(), curr_model_file_path)
        model.eval()
        valid_loss = 0.0
        for i, data_batch in enumerate(val_loader, 0):
            # data is a list of [inputs, labels]
            inputs, labels = data_batch
            
            # forward + backward + optimize
            outputs, grad_fn = model(inputs)
            loss = loss_fn(outputs, labels.squeeze(1))

            # print and save statistics and trained model every 10 mini-batches
            valid_loss += loss.item()
            if epoch % (epochs / 10) == 9 and i == len(val_loader) - 1:
                # print statistics
                print(f'[{epoch}, {i + 1:5d}] epoch end. VALIDATION loss: {valid_loss / len(val_loader):.3f}')

                # save statistics
                with open(curr_epoch_stats_file_path_eval, 'w') as f:
                    if os.stat(curr_epoch_stats_file_path_eval).st_size == 0:
                        stats_list = []
                    else: stats_list = json.load(f)
                    stats_list.append({
                        'iteration': i,
                        'epoch': epoch,
                        'training_loss': running_loss / len(train_loader),
                        'validation_loss': valid_loss / len(val_loader)
                    })
                    json.dump(stats_list, f, indent=4, separators=(',', ': '))
        writer.add_scalar('Loss/train', running_loss/len(train_loader), epoch)
        writer.add_scalar('Loss/val',valid_loss/len(val_loader), epoch)

    print('Finished Training')



if __name__ == '__main__':

    # Initialize model
    model = WebsiteClassifier(device='cpu', version=VERSION, classes=FILTERED_CLASSES)

    # Read dataset from csv file, split into train and test sets and create dataloaders 
    full_dataset = CSVDataset(
        '../data/URL-categorization-DFE.csv', model,
        classes=FILTERED_CLASSES, fetch_unfetched_urls=FETCH_UNFETCHED, max_rows=MAX_ROWS
    )
    print('data set muntat, primer element: ')
    print(full_dataset[0])
    print('length of page tensor0: ', len(full_dataset[0][0]))
    print('length of page tensor1: ', len(full_dataset[1][0]))
    print('length of page tensor2: ', len(full_dataset[2][0]))
   
    train_size = math.ceil(0.8 * len(full_dataset))
    test_size = len(full_dataset) - train_size
    train_dataset, test_dataset = random_split(full_dataset, [train_size, test_size])
    train_loader = DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True)
    test_loader = DataLoader(test_dataset, batch_size=BATCH_SIZE, shuffle=True)

    # Set loss criterion
    loss = torch.nn.CrossEntropyLoss()

    # Set opimizer type
    optimizer_type = torch.optim.Adam
    # optimizer = torch.optim.Adam(model.model.parameters(), lr=LEARNING_RATE)

    train(model=model.model, train_loader=train_loader,val_loader=test_loader, loss_fn=loss, optimizer_type=optimizer_type, version=VERSION)

    # trained_model_path = \
    #     '../trained_models/trained_model_' \
    #     + str(datetime.now().strftime("%Y%m%dT%H%M%S")) + '.pt'
    # os.makedirs(os.path.dirname(trained_model_path), exist_ok=True)
    # torch.save(model.model.state_dict(), trained_model_path)
