
# we have to see which pairs of (curlie, dfe) generate each tag
#  our_label -> (curlie_label, dfe_label)

# and which tags generate each pair of (curlie, dfe)
#  (curlie_label, dfe_label) -> our_label

# so that we can infer the tag of a given (curlie, dfe)

from typing import Dict
from original_w2v.model import WebsiteClassifier
import pandas as pd
from functools import reduce


class InferingWebpage:
    def __init__(self, url, our_tag=None, curlie_tags=[], dfe_tags=[]):
        self.url = url
        self.our_tag = our_tag
        self.curlie_tags = curlie_tags
        self.dfe_tags = dfe_tags

    def _tags_str(self, tags: Dict):
        ordered_tags = list(tags.items())
        ordered_tags.sort(key=lambda sc: sc[1], reverse=True)
        relevant_scores = list(filter(
            lambda score: score[1] >= 0.5, ordered_tags))[:3]
        if len(relevant_scores) == 1 and ordered_tags[1][1] < 0.25:
            relevant_scores.append(ordered_tags[1])
        return reduce(
            lambda a, b: ((a + ', ') if len(a) else '') +
            b[0],  # + ':' + str(round(b[1], 2)),
            relevant_scores, '')

    def __str__(self):
        return self.url + '\n  Our tag: ' + self.our_tag + \
            '\n  Curlie tags: ' + \
            self.__tags_str(self.curlie_tags) + \
            '\n  Dfe tags: ' + \
            self.__tags_str(self.dfe_tags)

    def __repr__(self):
        return self.__str__()


def add_combination(curr_tag_appearances):
    print(curr_tag + ' appearances:')
    print(curr_tag_appearances)
    # print(list(curr_tag_appearances.items()).sort(
    #     key=lambda sc: sc[1], reverse=True))
    combinations[curr_tag] = curr_tag_appearances


if __name__ == '__main__':
    # recorrem el csv del pollo i guardem webs amb tags nostres
    file_reader = pd.read_csv('../data/tagged_subpages_1.csv')
    urls = list(file_reader.iloc[1:, 0].values)
    tags = list(file_reader.iloc[1:, 1].values)
    our_tags = list(set(tags))
    webpages = []
    for i, url in enumerate(urls):
        w = InferingWebpage(url, tags[i])
        webpages.append(w)
    webpages.sort(key=lambda w: w.our_tag)
    w2v_curlie_model = WebsiteClassifier(device='cpu')
    curr_tag = webpages[0].our_tag
    print('Current Tag: ' + curr_tag + '\n')
    combinations = {tag: {} for tag in our_tags}
    curr_tag_appearances = {}
    for i, w in enumerate(webpages):
        if curr_tag != w.our_tag:
            add_combination(curr_tag_appearances)
            curr_tag = w.our_tag
            curr_tag_appearances = {}
            print('\nCurrent Tag: ' + curr_tag)
        print('(' + str(i+1) + '/' + str(len(webpages)) + ') - ' + w.url + ' :')
        w2v_w = w2v_curlie_model.fetch_website(w.url)
        if (w2v_w is None or not w2v_w.is_valid):
            continue
        scores, embeddings = w2v_curlie_model.predict(w2v_w)
        w.curlie_tags = scores
        tags_repr_ = w._tags_str(w.curlie_tags)
        print('\033[1m' + tags_repr_, end='')
        if tags_repr_ in curr_tag_appearances:
            curr_tag_appearances[tags_repr_] += 1
            print('   -   ' +
                  str(curr_tag_appearances[tags_repr_]) + ' times', end='')
        else:
            curr_tag_appearances[tags_repr_] = 1
        print('\033[0m\n', end='')
    add_combination(curr_tag_appearances)

    print('\nALL COMBINATIONS:\n')
    print(combinations)
